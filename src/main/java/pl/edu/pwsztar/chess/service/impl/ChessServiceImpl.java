package pl.edu.pwsztar.chess.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwsztar.chess.Converter;
import pl.edu.pwsztar.chess.domain.Point;
import pl.edu.pwsztar.chess.domain.RulesOfGame;
import pl.edu.pwsztar.chess.dto.FigureMoveDto;
import pl.edu.pwsztar.chess.service.ChessService;

@Service
@Transactional
public class ChessServiceImpl implements ChessService {
    private RulesOfGame king;
    private RulesOfGame queen;
    private RulesOfGame rook;
    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame pawn;
    private final Converter<Point, String> mapper;

    // ...

    public ChessServiceImpl(Converter<Point, String> mapper) {
        this.mapper = mapper;
        king = new RulesOfGame.King();
        queen = new RulesOfGame.Queen();
        rook = new RulesOfGame.Rook();
        bishop = new RulesOfGame.Bishop();
        knight = new RulesOfGame.Knight();
        pawn = new RulesOfGame.Pawn();

        // ...
    }

    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {

        // refaktoryzacja?
        switch (figureMoveDto.getType()) {
            case KING:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return king.isCorrectMove(mapper.convert(figureMoveDto.getSource()), mapper.convert(figureMoveDto.getDestination()));
            case QUEEN:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return queen.isCorrectMove(mapper.convert(figureMoveDto.getSource()), mapper.convert(figureMoveDto.getDestination()));
            case ROOK:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return rook.isCorrectMove(mapper.convert(figureMoveDto.getSource()), mapper.convert(figureMoveDto.getDestination()));
            case BISHOP:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return bishop.isCorrectMove(mapper.convert(figureMoveDto.getSource()), mapper.convert(figureMoveDto.getDestination()));
            case KNIGHT:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return knight.isCorrectMove(mapper.convert(figureMoveDto.getSource()), mapper.convert(figureMoveDto.getDestination()));
            case PAWN:
                // wywolaj konwerter punktow oraz popraw ponizszy kod
                return pawn.isCorrectMove(mapper.convert(figureMoveDto.getSource()), mapper.convert(figureMoveDto.getDestination()));
        }

        return false;
    }
}
